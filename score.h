#ifndef SCORE_H
#define SCORE_H

#include "types.h"
#include "routing.h"

struct score_t {
    float worst_link_tokens;
    float longest_route_hops;
    /* The static hops, and hops taken over links. */
    float total_hops;
    float total_route_hops;
    float total_route_hops_scal;
    float merged_static_hops;
    float merged_static_hops_scal;
    /* The total number of packets sent over the worst logical link. This 
     * provides a bound on the penalty calculation factor. */
    float link_penalty_lim;
    /* The total number of packets injected into the network at the worst port. */
    float inject_port_lim;
    /* The total number of packets ejected from the network at the worst port. */
    float eject_port_lim;
    /* The computed score. */
    float comp;
    /* Whether this score has already been used. */
    char used;
    /* The worst link stats. */
    int worst_link_id;
    /* The unmerged static hops. */
    float unmerged_static_hops;
    /* The unmerged dynamic hops. */
    float unmerged_dynamic_hops;
    /* Whether data can go in VC 0, or needs to take a high VC. */
    float tot_q0, tot_q;
    float max_non_q0;
    /* The number of VCs needed. */
    float vcs_needed;
};

int
linearize_link(int row, int col, int dir, int ncol);

int
linearize_node(int row, int col, int ncol);

void
print_throughput_limits(struct route_assignment_t *cand);

float
unassign_score_worst_link(struct route_t *rt, struct node_t *from, 
        struct node_t *to, struct score_t *score);

struct score_t *
score_route_assignment(struct route_assignment_t *cand);

void
score_route(struct route_assignment_t *cand, struct route_t *route, routing_func routing);

void
unscore_route(struct route_assignment_t *cand, struct route_t *route, routing_func routing);

GList *
get_deadlock_graph(struct route_assignment_t *cand);

GList *
get_program_graph(struct program_graph_t *prog);

/* Helper function for sorting routes */
int
route_comp_func(const struct route_t *a, const struct route_t *b);

int
check_route_deadlock(struct route_assignment_t *cand);

void
fix_route_deadlocks(struct route_assignment_t *cand);

/* This assigns a penalty that correlates with the linear score function. */
float 
linear_penalty(struct route_t *rt, struct node_t *from, struct node_t *to,
        struct score_t *score, float *params);

float
score_func_linear(struct score_t *score, float *params);

int
allocate_vcs_seq(struct route_assignment_t *assignment);
/* @brief: update the weight on mesh links using
 *         information from the given route.
 *         The route_hops from the route are used to determine
 *         which links should be updated
 *
 * @param links
 * @param route: the route to get route_hops from
 * @paran add: 0 = "remove/sub", otherwise "add"
 * @param ncol: number of columns in the mesh
 */

void 
update_links(struct link_t *links, struct route_t *route, char add, int ncol);

#endif
