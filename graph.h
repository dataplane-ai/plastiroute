#ifndef GRAPH_H
#define GRAPH_H

#include <glib-2.0/glib.h>
#include "types.h"

/* Offset to put all nodes into separate ID space than links. */
#define NODE_LINK_OFFSET 1000000000L

struct edge_t {
  int from;
  int to;
  void *ID;
  char tmp;
};

int
allocate_vcs_hops(struct route_assignment_t *route);

/* This takes in an edge list and removes all edges that are not part of a 
 * fully connected subgraph. It returns all edges that are part of cycles. */
GList *
prune_to_cyclic(GList *graph);

/* This adds an edge to a graph, checking for duplicates. */
GList          *
add_edge_nodup(GList * graph, int from, int to, void *ID);

/* This takes in an edge list and a pointer to an edge, and returns a list of 
 * all coincident edges. It is intended for use in the graph coloring phase of
 * deadlock avoidance. */
GList *
get_shared_edges(GList *graph, struct edge_t *edge);

/* This returns the count of shared edges for each edge in a graph. This is 
 * intended to be used as a heuristic for selecting an edge to enforce coloring
 * constraints on. */
int
count_shared_edges(GList *graph, struct edge_t *edge);

/* This finds the best "break point," which is here defined as a directed edge
 * in the program graph that is moved into a different VC. */
struct edge_t *
get_best_break_point(GList *graph);

/* This adds coloring edges to avoid endpoint buffer deadlock. */
GList *
add_endpoint_colors(struct route_assignment_t *route);

/* This adds coloring edges for VC allocation. */
GList *
add_deadlock_colors(GList *waits_holds, GList *colors);

/* This checks whether a cyclic deadlock graph contains cycles without nodes. */
int
network_only_cycles(GList *deadlock);

/* This checks whether a cyclic deadlock graph is a logical subgraph of the 
 * original program. */
int
is_dependency_subgraph(GList *deadlock, GList *program);

/* This checks whether the deadlock graph is fully static. */
int 
deadlock_is_static(GList *deadlock);

#endif

