#!/bin/bash
cd plastisim
sed -i.bak s_/DUT__g *.csv
HOSTNAME=`hostname`
SRUNDIR=`/bin/pwd | sed s_.*/scratch_/remote-scratch/${HOSTNAME}_`
/home/acrucker/plastiroute_test/plastiroute -n node.csv -l link.csv -y outlink.csv -z inlink.csv -r 20 -c 20 -A 1 -x 4 -e 12 -s 0 -i 10 -p 20 -O20 -t1 -d20 -G final.place -X /Top -q2 -g final.dot -v summary.csv -E15 -Z1 -Y1
