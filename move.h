#ifndef MOVE_H
#define MOVE_H

#include "score.h"
#include "types.h"
#include "routing.h"

int
assign_node_random(struct route_assignment_t *assign);

int
assign_node_spdram(struct route_assignment_t *assign);

void assign_drams(struct route_assignment_t *assign);

void
assign_all_routes_dijkstra(struct route_assignment_t *assign, routing_func routing);

void
unassign_all_routes(struct route_assignment_t *assign, routing_func routing);

/* This function merges all route hops into a single, unique set of multicast
 * routing decisions. */
void
merge_route_hops(struct route_assignment_t *route);

/* This assigns a single route to static (only if possible). */
void
try_assign_static_route(struct route_assignment_t *assign, struct route_t *route);

/* This reports whether a route can be made static at a given link. */
int
route_can_be_made_static(struct link_t *link, int route_id, int max_static, int type_id, int pre);

void
print_static_dynamic_routes(struct route_assignment_t *assign);

/* This function assigns static resources greedily, in decreasing penalty 
 * order. */
void
greedy_assign_static_routes(struct route_assignment_t *assign);

/* This picks the node that scores highest on the provided selection criterion,
 * and removes it. */
int
unassign_nodes_directed(struct route_assignment_t *assign, struct score_t *score, int count);

/* This picks a random node out of the set of assigned nodes, and unassigns it
 * and all its assigned routes. */
int
unassign_node_random(struct route_assignment_t *assign);

/* This picks a random node and assigns it to the closest spot to its neighbors. */
int
assign_node_directed(struct route_assignment_t *assign, int tgt_dist);

/* This loads a placement from a file. */
void
load_place_from_file(struct route_assignment_t *route, const char *file);

/* This executes a single step in the genetic algorithm. */
void
step_route_assignment(struct route_assignment_t *assign, struct score_t *score, routing_func routing, int use_dijkstra, int early);

#endif
