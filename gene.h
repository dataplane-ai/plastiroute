#ifndef GENE_H
#define GENE_H

#include "types.h"
#include "score.h"
#include "move.h"

#define MAX_PARAM 10

struct cand_pool_t {
    struct route_assignment_t **candidates;
    struct score_t **scores;
    float (*score_func)(struct score_t *score, float *params);
    float params[MAX_PARAM];
    int len;
};

struct cand_pool_t *
init_candidate_pool(struct route_assignment_t *init, int n);

/* This copies a candidate pool into a new pool of length n. The top N 
 * candidates (by summarized score) are duplicated a specified number of times,
 * and the remainder of the new pool is filled in with random value. If 
 * freeze is non-zero, the first duplicated topn value is frozen and will
 * not be modified in the next iteration. 
 * For example, len=10, topn=2, topn_dup=3, freeze = 1:
 *  + 3 of the best configuration, with the first immutable
 *  + 3 of the second-best, with the first immutable
 *  + 4 random values, with none immutable.
 * This ensures that there is never a regression the max between rounds. */
struct cand_pool_t *
winnow_candidate_pool(struct cand_pool_t *pool, int len,
        int topn, int topn_dup, char freeze, int iter);

void
free_candidate_pool(struct cand_pool_t *pool);

void
set_score_func_by_name(struct cand_pool_t *cand, const char *name);

void
set_param(struct cand_pool_t *cand, int n, float param);

void
init_place_all(struct cand_pool_t *pool);

float
score_all(struct cand_pool_t *pool);

void
step_all(struct cand_pool_t *pool, routing_func routing, int use_dijkstra, int early);

void
summarize_scores(struct cand_pool_t *pool);


#endif
