#ifndef ARBITER_H
#define ARBITER_H

#include "types.h"

struct arbiter_t {
	int arbiter_id;
	int row;
	int col;
	int direction;
	GList *route_ids;
};

void
first_common_ancestor(struct route_t *r1, 
		      struct route_t *r2, 
		      struct route_assignment_t *route);

GList*
all_route_nodes(struct route_t *r, int ncol);

void
print_arbiters(struct route_assignment_t *r);

void
visit_all_pairs(struct route_assignment_t *route);

#endif
