#ifndef TYPES_H
#define TYPES_H

#define NODE_TYPES 8
#define NUM_VCS 32
#define VC_OFFSET 1024

#include <stdint.h>
#include <stdlib.h>
#include <glib-2.0/glib.h>

struct node_t {
    int row;
    int col;
    int type;
    int prog_id;
    int bulk_off;
    int bulk_id;
    float unassign_score;
    char *name;
};

struct spdram_t {
  int bulk_id;
  int local;
  int count;
  int start_idx;
};

struct link_t {
    int total_weight;
    int total_weight_scal;
    /* total weight should be sum of all vc_weights */
    int vc_weights[NUM_VCS];
    GList *routes;
    int unique_static[2];
    int static_weight;
    int static_weight_scal;
    int id;
    int row, col, dir;
    int unique_dynamic;
};

struct mesh_loc_t {
    int row;
    int col;
};

struct hop_t {
    struct mesh_loc_t from;
    struct mesh_loc_t to;
    int vc;
    int is_static;
    int type;
    int equiv_id;
    int route_id;
    int penalty;
};

struct route_t {
    int penalty;
    int fanout;
    int nhops;
    int prog_id_from;
    int prog_id_to;
    int output_id;
    int vc;
    int ej_vc;
    int out_buf_id;

    int ctx_id;
    int type;
    int is_static;
    int ag_mc_link;
    int argin_link;
    int quality;

    char *name;
    char *dest_name;

    /* for use with valient routing only */
    GList *interm_nodes;

    /* explicit route hops */
    GList *route_hops;
    int route_id;
};

struct route_assignment_t {
    /* A hash of assigned nodes, keyed by prog_id. */
    GHashTable  *assigned_nodes;
    GList       *free_nodes[NODE_TYPES];
    GList       *unassigned_nodes;

    GList *assigned_routes;
    GList *unassigned_routes;
    GList *arbiters;

    GList *merged_hops;
    GList *merged_hops_static;

    GList *drams;

    struct link_t *links;

    char rand_dat[8];
    struct random_data rand_buf;

    int nnodes;
    int max_static;
    int max_static_scalar;
    int max_penalty;
    int max_vc;
    int n_row;
    int n_col;
    int ag_dup;
    int xc, yc;
    char frozen;
    // 0 for DRAMs on L-R only, 1 for DRAMs on all sides
    int dram_mode;
    int interior_merge;
};

struct chip_graph_t {
    GList *free_nodes[NODE_TYPES];
    int max_static;
    int max_static_scalar;
    int n_row;
    int n_col;
    int xc, yc;
    int max_vc;
    int ag_dup;
    int dram_mode;
    int interior_merge;
};

struct program_graph_t {
    GList *unassigned_nodes;
    GList *unassigned_routes;
    GList *drams;
};

/* routing function type declaration
 * @brief: routing functions do not alter the state of the links
 * @param links: information on the current state of links
 *               This information might be needed by adaptive algorithms
 * @param from: source node location
 * @param to: destination node location
 * @param num_cols: number of columns in the mesh
 * @param num_rows: number of rows in the mesh
 * @return the list of hops in the route
 */
typedef GList * (*routing_func)(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);

int
max(int a, int b);

void *
copy_route(const void *route, void *discard);

char
route_hop_to_direction(struct hop_t *hop);

int
route_hop_output_port(struct hop_t *hop);

void *
copy_node(const void *node, void *discard);

struct route_assignment_t *
new_route_assignment(struct program_graph_t *prog, 
                     struct chip_graph_t *chip);

void
free_route_assignment(struct route_assignment_t *rt);

struct route_assignment_t *
copy_route_assignment(struct route_assignment_t *route);

int 
allocate_vcs_avoid_conflicts(struct route_assignment_t *route, GList *conflicts);

struct chip_graph_t *
new_chip(int nrows, int ncols, int max_static, int max_static_scalar, int xc, int yc, int max_vc, const char* pattern, int pcu_pmu_ratio, int AGdup, int dram_mode, int interior_merge);

void
free_chip(struct chip_graph_t *chip);

struct program_graph_t *
load_program(const char *fn, const char *fl, const char *fs, const char *fd);

void
free_program(struct program_graph_t *prog);

int row_for_dram_id(int id, struct route_assignment_t *route);
int col_for_dram_id(int id, struct route_assignment_t *route);

void 
print_chip_graph(struct chip_graph_t *chip);
void 
print_prog_graph(struct program_graph_t *prog);

int rand_route(struct route_assignment_t  *assign);
void reseed_route(struct route_assignment_t *assign, int seed);

#endif
