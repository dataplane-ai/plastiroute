// DRAM29 -> 0 dims=List(16384) size=16384
// DRAM26 -> 16384 dims=List(16384, 64) size=1048576
// DRAM31 -> 1064960 dims=List(64) size=64
import scala.collection.mutable 
import java.io._ 
object tracer {
  val pws = mutable.Map[String, PrintWriter]()
  def main(args:Array[String]) =  {
    val dram = scala.collection.mutable.ArrayBuffer[Any]()
    dram ++= Array.ofDim[Any](16384) // DRAM29
    dram ++= Array.ofDim[Any](1048576) // DRAM26
    dram ++= Array.ofDim[Any](64) // DRAM31
    // TopController3
    ctrl {
      val DramAddress2719 = Some(1064960 * 4)
      val DramAddress152 = Some(0 * 4)
      val ArgIn3864 = Some(16384)
      val ArgIn3740 = Some(16384)
      val ArgIn3757 = Some(2)
      val DramAddress254 = Some(16384 * 4)
      // UnitController32
      ctrl {
        val Const3863 = 0
        val Const3756 = 0
        val Const3739 = 0
        val Const3765 = 1
        val Const3748 = 1024
        val Const3872 = 1024
        val EnabledLoadMem9132 = ArgIn3864.get
        val EnabledLoadMem9202 = ArgIn3740.get
        val EnabledLoadMem9222 = ArgIn3757.get
        // loop SeqPipe127
        val Counter3749 = Range(Const3739.asInstanceOf[Int], EnabledLoadMem9202.asInstanceOf[Int], Const3748.asInstanceOf[Int] * 1)
        val Counter3766 = Range(Const3756.asInstanceOf[Int], EnabledLoadMem9222.asInstanceOf[Int], Const3765.asInstanceOf[Int] * 1)
        Counter3766.foreach { SeqPipe127_iter0 =>
          Counter3749.foreach { SeqPipe127_iter1 =>
            val Const13584 = 0
            val Const3779 = 0
            val Const13586 = 1
            val Const3781 = 1
            val CounterIter3776 = SeqPipe127_iter1 + 0 * Const3748
            val Const3780 = 1024
            val Const13585 = 1024
            val Counter3873 = Range(Const3863.asInstanceOf[Int], EnabledLoadMem9132.asInstanceOf[Int], Const3872.asInstanceOf[Int] * 1)
            val CounterIter132 = SeqPipe127_iter1 + 0 * Const3748
            // UnitController142
            ctrl {
              val StreamOut144 = mutable.Queue(4096)
              val StreamOut143 = mutable.Queue[Any]()
              // UnitController146
              ctrl {
                val EnabledLoadMem9175 = DramAddress152.get
                val Const147 = 2
                val OpDef148 = eval(FixSla, List(CounterIter132, Const147))
                val OpDef156 = eval(FixAdd, List(OpDef148, EnabledLoadMem9175))
                val GlobalOutput8786 = OpDef156
                val ReadyValidGlobalInput8792 = GlobalOutput8786
                StreamOut143.enqueue(ReadyValidGlobalInput8792)
              }
              // UnitController2886
              ctrl {
                val EnabledLoadMem8834 = StreamOut144.dequeue
                trace(args(0) + "/EnabledLoadMem8834.trace", EnabledLoadMem8834)
                val EnabledLoadMem8810 = StreamOut143.dequeue
                trace(args(0) + "/EnabledLoadMem8810.trace", EnabledLoadMem8810)
              }
            }
            // loop StreamPipe223
            val Counter13587 = Range(Const13584.asInstanceOf[Int], Const13585.asInstanceOf[Int], Const13586.asInstanceOf[Int] * 1)
            Counter13587.foreach { StreamPipe223_iter0 =>
              val StreamOut229 = mutable.Queue(256)
              val StreamOut228 = mutable.Queue[Any]()
              val Counter3782 = Range(Const3779.asInstanceOf[Int], Const3780.asInstanceOf[Int], Const3781.asInstanceOf[Int] * 1)
              val CounterIter224 = StreamPipe223_iter0 + 0 * Const13586
              // UnitController231
              ctrl {
                val Const242 = 0
                val Const236 = 6
                val EnabledLoadMem9245 = DramAddress254.get
                val Const248 = 2
                val OpDef232 = eval(FixAdd, List(CounterIter3776, CounterIter224))
                val OpDef237 = eval(FixSla, List(OpDef232, Const236))
                val OpDef243 = eval(FixAdd, List(OpDef237, Const242))
                val OpDef249 = eval(FixSla, List(OpDef243, Const248))
                val OpDef258 = eval(FixAdd, List(OpDef249, EnabledLoadMem9245))
                val GlobalOutput8839 = OpDef258
                val ReadyValidGlobalInput8845 = GlobalOutput8839
                StreamOut228.enqueue(ReadyValidGlobalInput8845)
              }
              // UnitController2910
              ctrl {
                val EnabledLoadMem8887 = StreamOut229.dequeue
                trace(args(0) + "/EnabledLoadMem8887.trace", EnabledLoadMem8887)
                val EnabledLoadMem8863 = StreamOut228.dequeue
                trace(args(0) + "/EnabledLoadMem8863.trace", EnabledLoadMem8863)
              }
            }
          }
        }
        // UnitController2706
        ctrl {
          val StreamOut2708 = mutable.Queue(256)
          val StreamOut2707 = mutable.Queue[Any]()
          // UnitController2711
          ctrl {
            val Const3009 = 0
            val EnabledLoadMem9109 = DramAddress2719.get
            val OpDef2723 = eval(FixAdd, List(Const3009, EnabledLoadMem9109))
            val GlobalOutput8892 = OpDef2723
            val ReadyValidGlobalInput8898 = GlobalOutput8892
            StreamOut2707.enqueue(ReadyValidGlobalInput8898)
          }
          // UnitController2934
          ctrl {
            val EnabledLoadMem8940 = StreamOut2708.dequeue
            trace(args(0) + "/EnabledLoadMem8940.trace", EnabledLoadMem8940)
            val EnabledLoadMem8916 = StreamOut2707.dequeue
            trace(args(0) + "/EnabledLoadMem8916.trace", EnabledLoadMem8916)
          }
        }
      }
    }
    pws.foreach(_._2.close)
  }
  def trace(fileName:String, node:Any) = node match {
    case node:List[_] => getWriter(fileName).write(node.mkString(" ") + "\n")
    case node => getWriter(fileName).write(node.toString + "\n")
  }
  def getWriter(fileName:String) =  {
    pws.getOrElseUpdate(fileName, new PrintWriter(new File(fileName)))
  }
  def ctrl(block : => Unit) {
    block
  }
  

  import scala.util.{Try, Success, Failure}
  def parse(x:String):Any = {
    Try(x.toInt).recoverWith{ case _ => Try(x.toLong) }.recoverWith { case _ => Try(x.toFloat) }.recoverWith { case _ => Try(x.toDouble) }.get
  }

  def fvecLoad(mem:Array[Any], addr:Any):Any = addr match {
    case addrs:List[_] => addrs.map { a => mem(a.asInstanceOf[Int]) }
    case addr:Int => mem(addr)
  }

  def fvecStore(mem:Array[Any], addr:Any, data:Any) = (addr, data) match {
    case (addr:List[_],data:List[_]) => addr.zip(data).foreach { case (a, d) => mem(a.asInstanceOf[Int]) = d }
    case (addr:Int, data) => mem(addr) = data
    case (addr, data) => throw new Exception("Unknown "+ addr + " " + data + " type for fvecStore")
  }

  def vecLoad(mem:Array[_], addr:List[_]):Any =  {
    val idx::rest = addr
    if (rest.isEmpty) {
      idx match {
        case idx:List[_] => idx.map { vi => mem(vi.asInstanceOf[Int]) }
        case idx => mem(idx.asInstanceOf[Int])
      }
    } else {
      idx match {
        case idx:List[_] => idx.map { vi => vecLoad(mem(vi.asInstanceOf[Int]).asInstanceOf[Array[_]], rest) }
        case idx => vecLoad(mem(idx.asInstanceOf[Int]).asInstanceOf[Array[_]], rest)
      }
    }
  }

  def vecStore(mem:Array[_], addr:List[_], data:Any):Unit =  {
    val idx::rest = addr
    if (rest.isEmpty) {
      (idx, data) match {
        case (idx:List[_], data:List[_]) => 
          idx.zip(data).foreach { case (i, d) => 
            mem.asInstanceOf[Array[Any]](i.asInstanceOf[Int]) = d
          }
        case (idx,data) => 
          mem.asInstanceOf[Array[Any]](idx.asInstanceOf[Int]) = data
      }
    } else {
      val range = (idx, data) match {
        case (idx:List[_], data:List[_]) => idx.zip(data)
        case (idx,data) => List((idx, data))
      }
      range.foreach { case (i,d) => 
        vecStore(mem(i.asInstanceOf[Int]).asInstanceOf[Array[_]], rest, d)
      }
    }
  }
      
  
trait Enum
sealed trait Op extends Enum

sealed trait Op1 extends Op
sealed trait Op2 extends Op
sealed trait Op3 extends Op
sealed trait Op4 extends Op

sealed trait CompOp extends Op

sealed trait FixOp extends Op
case object FixAdd extends FixOp             with Op2
case object FixSub extends FixOp             with Op2
case object FixMul extends FixOp             with Op2
case object FixDiv extends FixOp             with Op2
case object FixMin extends FixOp             with Op2
case object FixMax extends FixOp             with Op2
case object FixLt  extends FixOp with CompOp with Op2
case object FixLeq extends FixOp with CompOp with Op2
case object FixGt  extends FixOp with CompOp with Op2
case object FixGeq extends FixOp with CompOp with Op2
case object FixEql extends FixOp with CompOp with Op2
case object FixNeq extends FixOp with CompOp with Op2
case object FixMod extends FixOp             with Op2
case object FixSra extends FixOp             with Op2
case object FixSla extends FixOp             with Op2
case object FixUsla extends FixOp            with Op2
case object FixNeg extends FixOp             with Op1
case object FixRandom extends FixOp          with Op1
case object FixUnif extends FixOp            with Op1
case object FixAbs extends FixOp             with Op1

sealed trait FltOp extends Op
case object FltAdd extends FltOp             with Op2
case object FltSub extends FltOp             with Op2
case object FltMul extends FltOp             with Op2
case object FltDiv extends FltOp             with Op2
case object FltMin extends FltOp             with Op2
case object FltMax extends FltOp             with Op2
case object FltLt  extends FltOp with CompOp with Op2
case object FltLeq extends FltOp with CompOp with Op2
case object FltGt  extends FltOp with CompOp with Op2
case object FltGeq extends FltOp with CompOp with Op2
case object FltEql extends FltOp with CompOp with Op2
case object FltNeq extends FltOp with CompOp with Op2
case object FltExp extends FltOp             with Op1
case object FltAbs extends FltOp             with Op1
case object FltLog extends FltOp             with Op1
case object FltSqr extends FltOp             with Op1
case object FltNeg extends FltOp             with Op1

sealed trait BitOp  extends Op
case object BitAnd  extends BitOp with Op2
case object BitOr   extends BitOp with Op2
case object BitNot  extends BitOp with Op1
case object BitXnor extends BitOp with Op2
case object BitXor  extends BitOp with Op2

case object MuxOp   extends Op with Op3
case object Bypass extends Op with Op1

val fixOps = List(FixAdd, FixSub, FixMul, FixDiv, FixMin, FixMax, FixLt, FixLeq, FixGt, FixGeq, FixEql, FixNeq, FixMod, FixSra, FixSla, FixUsla, FixNeg, FixRandom, FixUnif)
val fltOps = List(FltAdd, FltSub, FltMul, FltDiv, FltMin, FltMax, FltLt, FltGt, FltGeq, FltEql, FltNeq, FltExp, FltAbs, FltLog, FltSqr, FltNeg)
val bitOps = List(BitAnd, BitOr, BitNot, BitXnor, BitXor)
val otherOps = List(MuxOp, Bypass)

def ops = (fixOps ++ fltOps ++ bitOps ++ otherOps).toList
def compOps = ops.collect {case op:CompOp => op}
def fixCompOps = fixOps.collect {case op:CompOp => op}
def fltCompOps = fltOps.collect {case op:CompOp => op}

object ToInt {
  def unapply(x:Any):Option[Int] = x match {
    case x:Int => Some(x)
    case x:Float => Some(java.lang.Float.floatToIntBits(x))
    case _ => None
  }
}
object ToFloat {
  def unapply(x:Any):Option[Float] = x match {
    case x:Float => Some(x)
    case x:Int => Some(x.toFloat)
    case _ => None
  }
}
object ToBool {
  def unapply(x:Any):Option[Boolean] = x match {
    case x:Boolean => Some(x)
    case x:Int => Some(x > 0)
    case _ => None
  }
}

def eval(op:Op, ins:List[Any]):Any = {
  (op, ins) match {
    case (FixAdd   , ToInt(a)::ToInt(b)::_)     => (a + b)
    case (FixSub   , ToInt(a)::ToInt(b)::_)     => (a - b)
    case (FixMul   , ToInt(a)::ToInt(b)::_)     => (a * b)
    case (FixDiv   , ToInt(a)::ToInt(b)::_)     => (a / b)
    case (FixMin   , ToInt(a)::ToInt(b)::_)     => (Math.min(a, b))
    case (FixMax   , ToInt(a)::ToInt(b)::_)     => (Math.max(a, b))
    case (FixLt    , ToInt(a)::ToInt(b)::_)     => (a < b)
    case (FixLeq   , ToInt(a)::ToInt(b)::_)     => (a <= b)
    case (FixGt    , ToInt(a)::ToInt(b)::_)     => (a > b)
    case (FixGeq   , ToInt(a)::ToInt(b)::_)     => (a >= b)
    case (FixEql   , ToInt(a)::ToInt(b)::_)     => (a == b)
    case (FixNeq   , ToInt(a)::ToInt(b)::_)     => (a != b)
    case (FixMod   , ToInt(a)::ToInt(b)::_)     => (a % b)
    case (FixSra   , ToInt(a)::ToInt(b)::_)     => (a >> b)
    case (FixSla   , ToInt(a)::ToInt(b)::_)     => (a << b)
    case (FixUsla  , ToInt(a)::ToInt(b)::_)     => (a << b)
    case (FixNeg   , ToInt(a)::_)               => (-a)
    case (FixRandom, ToInt(a)::_)               => (0) //TODO
    case (FixUnif  , ToInt(a)::_)               => (0) //TODO

    case (FltAdd   , ToFloat(a)::ToFloat(b)::_) => (a + b)
    case (FltSub   , ToFloat(a)::ToFloat(b)::_) => (a - b)
    case (FltMul   , ToFloat(a)::ToFloat(b)::_) => (a * b)
    case (FltDiv   , ToFloat(a)::ToFloat(b)::_) => (a / b)
    case (FltMin   , ToFloat(a)::ToFloat(b)::_) => (Math.min(a , b))
    case (FltMax   , ToFloat(a)::ToFloat(b)::_) => (Math.max(a , b))
    case (FltLt    , ToFloat(a)::ToFloat(b)::_) => (a < b)
    case (FltLeq   , ToFloat(a)::ToFloat(b)::_) => (a <= b)
    case (FltGt    , ToFloat(a)::ToFloat(b)::_) => (a > b)
    case (FltGeq   , ToFloat(a)::ToFloat(b)::_) => (a >= b)
    case (FltEql   , ToFloat(a)::ToFloat(b)::_) => (a == b)
    case (FltNeq   , ToFloat(a)::ToFloat(b)::_) => (a != b)
    case (FltExp   , ToFloat(a)::ToFloat(b)::_) => (Math.exp(a))
    case (FltAbs   , ToFloat(a)::ToFloat(b)::_) => (Math.abs(a))
    case (FltLog   , ToFloat(a)::ToFloat(b)::_) => (Math.log(a))
    case (FltSqr   , ToFloat(a)::ToFloat(b)::_) => (Math.sqrt(a))
    case (FltNeg   , ToFloat(a)::ToFloat(b)::_) => (-a)

    case (BitAnd   , ToBool(a)::ToBool(b)::_)   => (a && b)
    case (BitOr    , ToBool(a)::ToBool(b)::_)   => (a || b)
    case (BitNot   , ToBool(a)::ToBool(b)::_)   => (!a)
    case (BitXnor  , ToBool(a)::ToBool(b)::_)   => (a == b)
    case (BitXor   , ToBool(a)::ToBool(b)::_)   => (a != b)

    case (Bypass   , a::_)                      => a
    case (MuxOp    , ToBool(true)::a::b::_)     => a
    case (MuxOp    , ToBool(false)::a::b::_)    => b
    case (op, (a:List[_])::(b:List[_])::(c:List[_])::rest) => (a,b,c).zipped.map { case (a,b,c) => eval(op, a::b::c::rest) }
    case (op, (a:List[_])::(b:List[_])::rest) => (a,b).zipped.map { case (a,b) => eval(op, a::b::rest) }
    case (op, a::(b:List[_])::(c:List[_])::rest) => (b,c).zipped.map { case (b,c) => eval(op, a::b::c::rest) }
    case (op, (a:List[_])::b::(c:List[_])::rest) => (a,c).zipped.map { case (a,c) => eval(op, a::b::c::rest) }
    case (op, (a:List[_])::rest) => a.map { a => eval(op, a::rest) }
    case (op, (a::(b:List[_])::rest)) => b.map { b => eval(op, a::b::rest) }

    case (op, ins) => throw new Exception("Don't know how to evaluate " + op + " ins=" + ins)
  }
}

}
