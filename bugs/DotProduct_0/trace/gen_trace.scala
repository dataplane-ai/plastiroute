// DRAM724055290 -> 0 dims=List(1048576) size=1048576
// DRAM18974240 -> 1048576 dims=List(1048576) size=1048576
import scala.collection.mutable 
import java.io._ 
object tracer {
  val pws = mutable.Map[String, PrintWriter]()
  def main(args:Array[String]) = {
    // Top0
    var Reg7:Any = None
    var Reg12:Any = None
    // SRAM23
    // SRAM24
    val FIFO33 = mutable.Queue[Any]()
    val FIFO34 = mutable.Queue[Any]()
    val FIFO36 = mutable.Queue[Any]()
    var Reg47:Any = None
    val FIFO82 = mutable.Queue[Any]()
    val FIFO83 = mutable.Queue[Any]()
    val FIFO85 = mutable.Queue[Any]()
    var Reg96:Any = None
    var Reg128:Any = None
    // HostInController4 {
      val DRAMAddr48 = 0 // DRAMAddr(DRAM724055290)
      Reg47 = DRAMAddr48
      val MemWrite49 = DRAMAddr48
      val DRAMAddr97 = 1048576 // DRAMAddr(DRAM18974240)
      Reg96 = DRAMAddr97
      val MemWrite98 = DRAMAddr97
    // } HostInController4
    // UnitController9 {
      val Const14 = 0
      val Const15 = 32768
      val Const16 = 1048576
      val Counter13 = (Const14 until Const16 by (Const15 * 1))
      Counter13.foreach { iter_Counter13 =>
        val Const130 = 0
        val Const131 = 1
        val Const132 = 32768
        val CounterIter21 = iter_Counter13 + 0 * Const15
        // UnitController26 {
          // UnitController30 {
            val Const63 = 0
            val Const64 = 1
            val Const65 = 32768
            // UnitController38 {
              val Const42 = 4
              val Const44 = true
              val Const45 = 64
              val Const46 = 0
              val Const52 = 131072
              val Const54 = true
              val OpDef41 = eval("FixMul")(CounterIter21,Const42)
              val OpDef43 = eval("FixToFix")(OpDef41,Const44,Const45,Const46)
              val MemRead50 = Reg47
              val OpDef51 = eval("FixAdd")(OpDef43,MemRead50)
              FIFO33.enqueue(OpDef51)
              val MemWrite55 = OpDef51
              FIFO34.enqueue(Const52)
              val MemWrite56 = Const52
            // } UnitController38
            // FringeDenseLoad58
            trace("FringeDenseLoad58_size.trace", MemWrite56)
            trace("FringeDenseLoad58_offset.trace", MemWrite55)
            val MemRead59 = FIFO33.dequeue()
            val MemRead60 = FIFO34.dequeue()
            // MemWrite61
            val Counter62 = (Const63 until Const65 by (Const64 * 16))
            Counter62.foreach { iter_Counter62 =>
              val Const74 = 15
              val Const76 = 4
              val CounterIter70 = List.tabulate(16) { i => iter_Counter62 + i * Const64 }
              // MemRead72
              // OpDef73
              // OpDef75
              // BankedWrite77
            }
          // } UnitController30
          // UnitController79 {
            val Const112 = 0
            val Const113 = 1
            val Const114 = 32768
            // UnitController87 {
              val Const91 = 4
              val Const93 = true
              val Const94 = 64
              val Const95 = 0
              val Const101 = 131072
              val Const103 = true
              val OpDef90 = eval("FixMul")(CounterIter21,Const91)
              val OpDef92 = eval("FixToFix")(OpDef90,Const93,Const94,Const95)
              val MemRead99 = Reg96
              val OpDef100 = eval("FixAdd")(OpDef92,MemRead99)
              FIFO82.enqueue(OpDef100)
              val MemWrite104 = OpDef100
              FIFO83.enqueue(Const101)
              val MemWrite105 = Const101
            // } UnitController87
            // FringeDenseLoad107
            trace("FringeDenseLoad107_size.trace", MemWrite105)
            trace("FringeDenseLoad107_offset.trace", MemWrite104)
            val MemRead108 = FIFO82.dequeue()
            val MemRead109 = FIFO83.dequeue()
            // MemWrite110
            val Counter111 = (Const112 until Const114 by (Const113 * 16))
            Counter111.foreach { iter_Counter111 =>
              val Const123 = 15
              val Const125 = 4
              val CounterIter119 = List.tabulate(16) { i => iter_Counter111 + i * Const113 }
              // MemRead121
              // OpDef122
              // OpDef124
              // BankedWrite126
            }
          // } UnitController79
        // } UnitController26
        val Counter129 = (Const130 until Const132 by (Const131 * 16))
        Counter129.foreach { iter_Counter129 =>
          val Const140 = 15
          val Const142 = 4
          val Const146 = 0
          val CounterIter137 = List.tabulate(16) { i => iter_Counter129 + i * Const131 }
          // OpDef139
          // OpDef141
          // BankedRead143
          // BankedRead144
          // OpDef145
          // OpDef147
          // RegAccumOp148
          // MemWrite149
        }
        // UnitController151 {
          val Const156 = 0
          // MemRead154
          // OpDef155
          // MemRead157
          // OpDef158
          // OpDef159
          // MemWrite160
        // } UnitController151
      }
      // UnitController162 {
        // MemRead165
        // MemWrite166
      // } UnitController162
    // } UnitController9
    // HostOutController168 {
      // HostRead171
      // MemRead172
    // } HostOutController168
    pws.foreach(_._2.close)
  }
  def trace(fileName:String, node:Any) = node match {
    case node:List[_] => getWriter(fileName).write(node.mkString(" ") + "\n")
    case node => getWriter(fileName).write(node.toString + "\n")
  }
  def getWriter(fileName:String) =  {
    pws.getOrElseUpdate(fileName, new PrintWriter(new File(fileName)))
  }
  
  def eval(op:String)(inputs:Any*):Any = {
    val vecs = inputs.collect { case vec:List[_] => vec }
    val vecSize = assertUnify(vecs) { _.size }.getOrElse(1)
    val vins = inputs.map {
      case vec:List[_] => vec
      case e => List.fill(vecSize)(e)
    }
    val vres = List.tabulate(vecSize) { i =>
      val ins = vins.map { _(i) }
      evalOp(op)(ins)
    }
    if (vecs.size == 0) {
      vres.head
    } else {
      vres
    }
  }
  def evalOp(op:String)(inputs:Seq[Any]):Any = {
    (op, inputs) match {
      case ("FixAdd", Seq(a:Int, b:Int)) => a + b
      case ("FixAdd", Seq(a:Float, b:Float)) => a * b
      case ("FixMul", Seq(a:Int, b:Int)) => a * b
      case ("FixMul", Seq(a:Float, b:Float)) => a * b
      case ("FixFMA", Seq(a:Int, b:Int, c:Int)) => a * b + c
      case ("FixFMA", Seq(a:Float, b:Float, c:Float)) => a * b + c
      case ("FixToFix", ins) => ins(0)
      case op => throw new Exception(s"Don't know how to eval $op")
    }
  }

  def assertUnify[A,B](list:Iterable[A])(lambda:A => B):Option[B] = {
    val res = list.map(a => lambda(a))
    assert(res.toSet.size<=1, s"$list doesnt have the same evaluation = $res")
    res.headOption
  }

}
