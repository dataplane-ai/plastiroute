// DRAM21 -> 0 dims=List(512, 512) size=262144
// DRAM16 -> 262144 dims=List(256, 512) size=131072
// DRAM26 -> 393216 dims=List(256, 512) size=131072
import scala.collection.mutable 
import java.io._ 
object tracer {
  val pws = mutable.Map[String, PrintWriter]()
  def main(args:Array[String]) =  {
    val dram = scala.collection.mutable.ArrayBuffer[Any]()
    dram ++= Array.ofDim[Any](262144) // DRAM21
    dram ++= Array.ofDim[Any](131072) // DRAM16
    dram ++= Array.ofDim[Any](131072) // DRAM26
    // TopController3
    ctrl {
      val DramAddress2614 = Some(393216 * 4)
      val DramAddress152 = Some(0 * 4)
      val DramAddress342 = Some(262144 * 4)
      val ArgIn3723 = Some(512)
      val ArgIn3528 = Some(512)
      val ArgIn3076 = Some(512)
      val ArgIn3098 = Some(256)
      val ArgIn3634 = Some(512)
      val ArgIn3681 = Some(256)
      // UnitController27
      ctrl {
        val EnabledLoadMem8345 = ArgIn3681.get
        val EnabledLoadMem8170 = ArgIn3098.get
        val Const3680 = 0
        val Const3097 = 0
        val Const3689 = 128
        val Const3106 = 128
        // loop MetaPipe40
        val Counter3107 = Range(Const3097.asInstanceOf[Int], EnabledLoadMem8170.asInstanceOf[Int], Const3106.asInstanceOf[Int] * 1)
        Counter3107.foreach { MetaPipe40_iter0 =>
          val EnabledLoadMem8256 = ArgIn3634.get
          val EnabledLoadMem8146 = ArgIn3076.get
          val Const3667 = 0
          val Const3075 = 0
          val Counter3690 = Range(Const3680.asInstanceOf[Int], EnabledLoadMem8345.asInstanceOf[Int], Const3689.asInstanceOf[Int] * 1)
          val Const3670 = 256
          val Const3084 = 256
          val CounterIter3115 = MetaPipe40_iter0 + 0 * Const3106
          val CounterIter41 = MetaPipe40_iter0 + 0 * Const3106
          // loop MetaPipe57
          val Counter3085 = Range(Const3075.asInstanceOf[Int], EnabledLoadMem8146.asInstanceOf[Int], Const3084.asInstanceOf[Int] * 1)
          Counter3085.foreach { MetaPipe57_iter0 =>
            val EnabledLoadMem8454 = ArgIn3723.get
            val EnabledLoadMem8247 = ArgIn3634.get
            val EnabledLoadMem8065 = ArgIn3528.get
            val Counter3671 = Range(Const3667.asInstanceOf[Int], EnabledLoadMem8256.asInstanceOf[Int], Const3670.asInstanceOf[Int] * 1)
            val Const3960 = 0
            val Const3729 = 0
            val Const3633 = 0
            val Const3527 = 0
            val Const3119 = 0
            val Const3962 = 1
            val Const3121 = 1
            val Const3732 = 256
            val Const3642 = 256
            val Const3536 = 256
            val CounterIter3093 = MetaPipe57_iter0 + 0 * Const3084
            val Const3961 = 128
            val Const3120 = 128
            val CounterIter58 = MetaPipe57_iter0 + 0 * Const3084
            // loop MetaPipe76
            val Counter3537 = Range(Const3527.asInstanceOf[Int], EnabledLoadMem8065.asInstanceOf[Int], Const3536.asInstanceOf[Int] * 1)
            Counter3537.foreach { MetaPipe76_iter0 =>
              val Counter3733 = Range(Const3729.asInstanceOf[Int], EnabledLoadMem8454.asInstanceOf[Int], Const3732.asInstanceOf[Int] * 1)
              val Counter3643 = Range(Const3633.asInstanceOf[Int], EnabledLoadMem8247.asInstanceOf[Int], Const3642.asInstanceOf[Int] * 1)
              val Const10557 = 0
              val Const10196 = 0
              val Const3655 = 0
              val Const3699 = 0
              val Const3701 = 1
              val Const3657 = 1
              val Const10198 = 1
              val Const10559 = 1
              val Const10197 = 256
              val Const3656 = 256
              val Const3700 = 128
              val Const10558 = 128
              val CounterIter77 = MetaPipe76_iter0 + 0 * Const3536
              val CounterIter3651 = MetaPipe76_iter0 + 0 * Const3536
              // loop StreamPipe122
              val Counter10199 = Range(Const10196.asInstanceOf[Int], Const10197.asInstanceOf[Int], Const10198.asInstanceOf[Int] * 1)
              Counter10199.foreach { StreamPipe122_iter0 =>
                val StreamOut128 = mutable.Queue(1024)
                val StreamOut127 = mutable.Queue[Any]()
                val Counter3658 = Range(Const3655.asInstanceOf[Int], Const3656.asInstanceOf[Int], Const3657.asInstanceOf[Int] * 1)
                val CounterIter123 = StreamPipe122_iter0 + 0 * Const10198
                // UnitController130
                ctrl {
                  val EnabledLoadMem8279 = DramAddress152.get
                  val EnabledLoadMem8252 = ArgIn3634.get
                  val OpDef131 = eval(FixAdd, List(CounterIter3651, CounterIter123))
                  val Const146 = 2
                  val OpDef137 = eval(FixMul, List(OpDef131, EnabledLoadMem8252))
                  val OpDef142 = eval(FixAdd, List(OpDef137, CounterIter58))
                  val OpDef147 = eval(FixSla, List(OpDef142, Const146))
                  val OpDef156 = eval(FixAdd, List(OpDef147, EnabledLoadMem8279))
                  val GlobalOutput7542 = OpDef156
                  val ReadyValidGlobalInput7548 = GlobalOutput7542
                  StreamOut127.enqueue(ReadyValidGlobalInput7548)
                }
                // UnitController2737
                ctrl {
                  val EnabledLoadMem7590 = StreamOut128.dequeue
                  trace(args(0) + "/EnabledLoadMem7590.trace", EnabledLoadMem7590)
                  val EnabledLoadMem7566 = StreamOut127.dequeue
                  trace(args(0) + "/EnabledLoadMem7566.trace", EnabledLoadMem7566)
                }
              }
              // loop MetaPipe257
              val Counter10560 = Range(Const10557.asInstanceOf[Int], Const10558.asInstanceOf[Int], Const10559.asInstanceOf[Int] * 1)
              Counter10560.foreach { MetaPipe257_iter0 =>
                var Reg263:Option[Any] = None
                val Const3711 = 0
                val Const3712 = 1
                val Const3713 = 1
                val Counter3702 = Range(Const3699.asInstanceOf[Int], Const3700.asInstanceOf[Int], Const3701.asInstanceOf[Int] * 1)
                val CounterIter258 = MetaPipe257_iter0 + 0 * Const10559
                // UnitController264
                ctrl {
                  val OpDef265 = eval(FixAdd, List(CounterIter41, CounterIter258))
                  val GlobalOutput8350 = OpDef265
                  val ReadyValidGlobalInput8355 = GlobalOutput8350
                  // x5667_x5660 = EnabledStoreMem8363(Reg263,None,ReadyValidGlobalInput8355,DataValid8360)
                  Reg263 = Some(ReadyValidGlobalInput8355)
                }
                // loop StreamPipe308
                val Counter3714 = Range(Const3711.asInstanceOf[Int], Const3712.asInstanceOf[Int], Const3713.asInstanceOf[Int] * 1)
                Counter3714.foreach { StreamPipe308_iter0 =>
                  val StreamOut314 = mutable.Queue(1024)
                  val StreamOut313 = mutable.Queue[Any]()
                  val CounterIter309 = StreamPipe308_iter0 + 0 * Const3713
                  // UnitController316
                  ctrl {
                    val EnabledLoadMem8477 = DramAddress342.get
                    val EnabledLoadMem8449 = ArgIn3723.get
                    val EnabledLoadMem8372 = Reg263.get
                    val Const336 = 2
                    val OpDef320 = eval(FixAdd, List(EnabledLoadMem8372, CounterIter309))
                    val OpDef327 = eval(FixMul, List(OpDef320, EnabledLoadMem8449))
                    val OpDef332 = eval(FixAdd, List(OpDef327, CounterIter77))
                    val OpDef337 = eval(FixSla, List(OpDef332, Const336))
                    val OpDef346 = eval(FixAdd, List(OpDef337, EnabledLoadMem8477))
                    val GlobalOutput7595 = OpDef346
                    val ReadyValidGlobalInput7601 = GlobalOutput7595
                    StreamOut313.enqueue(ReadyValidGlobalInput7601)
                  }
                  // UnitController2761
                  ctrl {
                    val EnabledLoadMem7643 = StreamOut314.dequeue
                    trace(args(0) + "/EnabledLoadMem7643.trace", EnabledLoadMem7643)
                    val EnabledLoadMem7619 = StreamOut313.dequeue
                    trace(args(0) + "/EnabledLoadMem7619.trace", EnabledLoadMem7619)
                  }
                }
              }
            }
            // loop StreamPipe2583
            val Counter3963 = Range(Const3960.asInstanceOf[Int], Const3961.asInstanceOf[Int], Const3962.asInstanceOf[Int] * 1)
            Counter3963.foreach { StreamPipe2583_iter0 =>
              val StreamOut2589 = mutable.Queue(1024)
              val StreamOut2588 = mutable.Queue[Any]()
              val Counter3122 = Range(Const3119.asInstanceOf[Int], Const3120.asInstanceOf[Int], Const3121.asInstanceOf[Int] * 1)
              val CounterIter3130 = StreamPipe2583_iter0 + 0 * Const3962
              // UnitController2592
              ctrl {
                val EnabledLoadMem8193 = DramAddress2614.get
                val EnabledLoadMem8151 = ArgIn3076.get
                val OpDef2593 = eval(FixAdd, List(CounterIter3115, CounterIter3130))
                val Const2608 = 2
                val OpDef2599 = eval(FixMul, List(OpDef2593, EnabledLoadMem8151))
                val OpDef2604 = eval(FixAdd, List(OpDef2599, CounterIter3093))
                val OpDef2609 = eval(FixSla, List(OpDef2604, Const2608))
                val OpDef2618 = eval(FixAdd, List(OpDef2609, EnabledLoadMem8193))
                val GlobalOutput7648 = OpDef2618
                val ReadyValidGlobalInput7654 = GlobalOutput7648
                StreamOut2588.enqueue(ReadyValidGlobalInput7654)
              }
              // UnitController2785
              ctrl {
                val EnabledLoadMem7696 = StreamOut2589.dequeue
                trace(args(0) + "/EnabledLoadMem7696.trace", EnabledLoadMem7696)
                val EnabledLoadMem7672 = StreamOut2588.dequeue
                trace(args(0) + "/EnabledLoadMem7672.trace", EnabledLoadMem7672)
              }
            }
          }
        }
      }
    }
    pws.foreach(_._2.close)
  }
  def trace(fileName:String, node:Any) = node match {
    case node:List[_] => getWriter(fileName).write(node.mkString(" ") + "\n")
    case node => getWriter(fileName).write(node.toString + "\n")
  }
  def getWriter(fileName:String) =  {
    pws.getOrElseUpdate(fileName, new PrintWriter(new File(fileName)))
  }
  def ctrl(block : => Unit) {
    block
  }
  

  import scala.util.{Try, Success, Failure}
  def parse(x:String):Any = {
    Try(x.toInt).recoverWith{ case _ => Try(x.toLong) }.recoverWith { case _ => Try(x.toFloat) }.recoverWith { case _ => Try(x.toDouble) }.get
  }

  def fvecLoad(mem:Array[Any], addr:Any):Any = addr match {
    case addrs:List[_] => addrs.map { a => mem(a.asInstanceOf[Int]) }
    case addr:Int => mem(addr)
  }

  def fvecStore(mem:Array[Any], addr:Any, data:Any) = (addr, data) match {
    case (addr:List[_],data:List[_]) => addr.zip(data).foreach { case (a, d) => mem(a.asInstanceOf[Int]) = d }
    case (addr:Int, data) => mem(addr) = data
    case (addr, data) => throw new Exception("Unknown "+ addr + " " + data + " type for fvecStore")
  }

  def vecLoad(mem:Array[_], addr:List[_]):Any =  {
    val idx::rest = addr
    if (rest.isEmpty) {
      idx match {
        case idx:List[_] => idx.map { vi => mem(vi.asInstanceOf[Int]) }
        case idx => mem(idx.asInstanceOf[Int])
      }
    } else {
      idx match {
        case idx:List[_] => idx.map { vi => vecLoad(mem(vi.asInstanceOf[Int]).asInstanceOf[Array[_]], rest) }
        case idx => vecLoad(mem(idx.asInstanceOf[Int]).asInstanceOf[Array[_]], rest)
      }
    }
  }

  def vecStore(mem:Array[_], addr:List[_], data:Any):Unit =  {
    val idx::rest = addr
    if (rest.isEmpty) {
      (idx, data) match {
        case (idx:List[_], data:List[_]) => 
          idx.zip(data).foreach { case (i, d) => 
            mem.asInstanceOf[Array[Any]](i.asInstanceOf[Int]) = d
          }
        case (idx,data) => 
          mem.asInstanceOf[Array[Any]](idx.asInstanceOf[Int]) = data
      }
    } else {
      val range = (idx, data) match {
        case (idx:List[_], data:List[_]) => idx.zip(data)
        case (idx,data) => List((idx, data))
      }
      range.foreach { case (i,d) => 
        vecStore(mem(i.asInstanceOf[Int]).asInstanceOf[Array[_]], rest, d)
      }
    }
  }
      
  
trait Enum
sealed trait Op extends Enum

sealed trait Op1 extends Op
sealed trait Op2 extends Op
sealed trait Op3 extends Op
sealed trait Op4 extends Op

sealed trait CompOp extends Op

sealed trait FixOp extends Op
case object FixAdd extends FixOp             with Op2
case object FixSub extends FixOp             with Op2
case object FixMul extends FixOp             with Op2
case object FixDiv extends FixOp             with Op2
case object FixMin extends FixOp             with Op2
case object FixMax extends FixOp             with Op2
case object FixLt  extends FixOp with CompOp with Op2
case object FixLeq extends FixOp with CompOp with Op2
case object FixGt  extends FixOp with CompOp with Op2
case object FixGeq extends FixOp with CompOp with Op2
case object FixEql extends FixOp with CompOp with Op2
case object FixNeq extends FixOp with CompOp with Op2
case object FixMod extends FixOp             with Op2
case object FixSra extends FixOp             with Op2
case object FixSla extends FixOp             with Op2
case object FixUsla extends FixOp            with Op2
case object FixNeg extends FixOp             with Op1
case object FixRandom extends FixOp          with Op1
case object FixUnif extends FixOp            with Op1
case object FixAbs extends FixOp             with Op1

sealed trait FltOp extends Op
case object FltAdd extends FltOp             with Op2
case object FltSub extends FltOp             with Op2
case object FltMul extends FltOp             with Op2
case object FltDiv extends FltOp             with Op2
case object FltMin extends FltOp             with Op2
case object FltMax extends FltOp             with Op2
case object FltLt  extends FltOp with CompOp with Op2
case object FltLeq extends FltOp with CompOp with Op2
case object FltGt  extends FltOp with CompOp with Op2
case object FltGeq extends FltOp with CompOp with Op2
case object FltEql extends FltOp with CompOp with Op2
case object FltNeq extends FltOp with CompOp with Op2
case object FltExp extends FltOp             with Op1
case object FltAbs extends FltOp             with Op1
case object FltLog extends FltOp             with Op1
case object FltSqr extends FltOp             with Op1
case object FltNeg extends FltOp             with Op1

sealed trait BitOp  extends Op
case object BitAnd  extends BitOp with Op2
case object BitOr   extends BitOp with Op2
case object BitNot  extends BitOp with Op1
case object BitXnor extends BitOp with Op2
case object BitXor  extends BitOp with Op2

case object MuxOp   extends Op with Op3
case object Bypass extends Op with Op1

val fixOps = List(FixAdd, FixSub, FixMul, FixDiv, FixMin, FixMax, FixLt, FixLeq, FixGt, FixGeq, FixEql, FixNeq, FixMod, FixSra, FixSla, FixUsla, FixNeg, FixRandom, FixUnif)
val fltOps = List(FltAdd, FltSub, FltMul, FltDiv, FltMin, FltMax, FltLt, FltGt, FltGeq, FltEql, FltNeq, FltExp, FltAbs, FltLog, FltSqr, FltNeg)
val bitOps = List(BitAnd, BitOr, BitNot, BitXnor, BitXor)
val otherOps = List(MuxOp, Bypass)

def ops = (fixOps ++ fltOps ++ bitOps ++ otherOps).toList
def compOps = ops.collect {case op:CompOp => op}
def fixCompOps = fixOps.collect {case op:CompOp => op}
def fltCompOps = fltOps.collect {case op:CompOp => op}

object ToInt {
  def unapply(x:Any):Option[Int] = x match {
    case x:Int => Some(x)
    case x:Float => Some(java.lang.Float.floatToIntBits(x))
    case _ => None
  }
}
object ToFloat {
  def unapply(x:Any):Option[Float] = x match {
    case x:Float => Some(x)
    case x:Int => Some(x.toFloat)
    case _ => None
  }
}
object ToBool {
  def unapply(x:Any):Option[Boolean] = x match {
    case x:Boolean => Some(x)
    case x:Int => Some(x > 0)
    case _ => None
  }
}

def eval(op:Op, ins:List[Any]):Any = {
  (op, ins) match {
    case (FixAdd   , ToInt(a)::ToInt(b)::_)     => (a + b)
    case (FixSub   , ToInt(a)::ToInt(b)::_)     => (a - b)
    case (FixMul   , ToInt(a)::ToInt(b)::_)     => (a * b)
    case (FixDiv   , ToInt(a)::ToInt(b)::_)     => (a / b)
    case (FixMin   , ToInt(a)::ToInt(b)::_)     => (Math.min(a, b))
    case (FixMax   , ToInt(a)::ToInt(b)::_)     => (Math.max(a, b))
    case (FixLt    , ToInt(a)::ToInt(b)::_)     => (a < b)
    case (FixLeq   , ToInt(a)::ToInt(b)::_)     => (a <= b)
    case (FixGt    , ToInt(a)::ToInt(b)::_)     => (a > b)
    case (FixGeq   , ToInt(a)::ToInt(b)::_)     => (a >= b)
    case (FixEql   , ToInt(a)::ToInt(b)::_)     => (a == b)
    case (FixNeq   , ToInt(a)::ToInt(b)::_)     => (a != b)
    case (FixMod   , ToInt(a)::ToInt(b)::_)     => (a % b)
    case (FixSra   , ToInt(a)::ToInt(b)::_)     => (a >> b)
    case (FixSla   , ToInt(a)::ToInt(b)::_)     => (a << b)
    case (FixUsla  , ToInt(a)::ToInt(b)::_)     => (a << b)
    case (FixNeg   , ToInt(a)::_)               => (-a)
    case (FixRandom, ToInt(a)::_)               => (0) //TODO
    case (FixUnif  , ToInt(a)::_)               => (0) //TODO

    case (FltAdd   , ToFloat(a)::ToFloat(b)::_) => (a + b)
    case (FltSub   , ToFloat(a)::ToFloat(b)::_) => (a - b)
    case (FltMul   , ToFloat(a)::ToFloat(b)::_) => (a * b)
    case (FltDiv   , ToFloat(a)::ToFloat(b)::_) => (a / b)
    case (FltMin   , ToFloat(a)::ToFloat(b)::_) => (Math.min(a , b))
    case (FltMax   , ToFloat(a)::ToFloat(b)::_) => (Math.max(a , b))
    case (FltLt    , ToFloat(a)::ToFloat(b)::_) => (a < b)
    case (FltLeq   , ToFloat(a)::ToFloat(b)::_) => (a <= b)
    case (FltGt    , ToFloat(a)::ToFloat(b)::_) => (a > b)
    case (FltGeq   , ToFloat(a)::ToFloat(b)::_) => (a >= b)
    case (FltEql   , ToFloat(a)::ToFloat(b)::_) => (a == b)
    case (FltNeq   , ToFloat(a)::ToFloat(b)::_) => (a != b)
    case (FltExp   , ToFloat(a)::ToFloat(b)::_) => (Math.exp(a))
    case (FltAbs   , ToFloat(a)::ToFloat(b)::_) => (Math.abs(a))
    case (FltLog   , ToFloat(a)::ToFloat(b)::_) => (Math.log(a))
    case (FltSqr   , ToFloat(a)::ToFloat(b)::_) => (Math.sqrt(a))
    case (FltNeg   , ToFloat(a)::ToFloat(b)::_) => (-a)

    case (BitAnd   , ToBool(a)::ToBool(b)::_)   => (a && b)
    case (BitOr    , ToBool(a)::ToBool(b)::_)   => (a || b)
    case (BitNot   , ToBool(a)::ToBool(b)::_)   => (!a)
    case (BitXnor  , ToBool(a)::ToBool(b)::_)   => (a == b)
    case (BitXor   , ToBool(a)::ToBool(b)::_)   => (a != b)

    case (Bypass   , a::_)                      => a
    case (MuxOp    , ToBool(true)::a::b::_)     => a
    case (MuxOp    , ToBool(false)::a::b::_)    => b
    case (op, (a:List[_])::(b:List[_])::(c:List[_])::rest) => (a,b,c).zipped.map { case (a,b,c) => eval(op, a::b::c::rest) }
    case (op, (a:List[_])::(b:List[_])::rest) => (a,b).zipped.map { case (a,b) => eval(op, a::b::rest) }
    case (op, a::(b:List[_])::(c:List[_])::rest) => (b,c).zipped.map { case (b,c) => eval(op, a::b::c::rest) }
    case (op, (a:List[_])::b::(c:List[_])::rest) => (a,c).zipped.map { case (a,c) => eval(op, a::b::c::rest) }
    case (op, (a:List[_])::rest) => a.map { a => eval(op, a::rest) }
    case (op, (a::(b:List[_])::rest)) => b.map { b => eval(op, a::b::rest) }

    case (op, ins) => throw new Exception("Don't know how to evaluate " + op + " ins=" + ins)
  }
}

}
