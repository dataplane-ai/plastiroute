#include "gene.h"

#include "assert.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "float.h"
#include "routing.h"
#include "omp.h"
//#include "pthread.h"

#define THREADS 1

extern routing_func routing_f;

struct cand_pool_t *
init_candidate_pool(struct route_assignment_t *init, int n) {
  int             i;
  struct cand_pool_t *ret;

  ret = malloc(sizeof(struct cand_pool_t));
  assert(ret);

  ret->candidates = malloc(sizeof(struct route_assignment_t *) * n);
  assert(ret->candidates);
  ret->scores = calloc(sizeof(struct score_t *), n);
  assert(ret->scores);

  if (init) {
//#pragma omp parallel for schedule(dynamic)
#pragma omp parallel for schedule(dynamic)
    for (i = 0; i < n; i++) {
      //printf("Init with %d threads. This is thread %d.\n", omp_get_num_threads(), omp_get_thread_num());
      ret->candidates[i] = copy_route_assignment(init);
      reseed_route(ret->candidates[i], i);
    }
  }

  ret->len = n;

  return ret;
}

int
get_next_low_score(struct cand_pool_t *pool) {
  float           min;
  int             ret,
                  i;
  min = FLT_MAX;
  ret = -1;

  for (i = 0; i < pool->len; i++) {
    if (pool->scores[i]->used)
      continue;
    if (pool->scores[i]->comp > min)
      continue;
    min = pool->scores[i]->comp;
    ret = i;
  }

  assert(ret >= 0);
  pool->scores[ret]->used = 1;
  return ret;
}

void
step_all(struct cand_pool_t *pool, routing_func routing, int use_dijkstra, int early) {
  int             i;
//#pragma omp parallel for schedule(guided)
#pragma omp parallel for schedule(dynamic)
  for (i = 0; i < pool->len; i++) {
    //printf("Stepping  with %d threads. This is thread %d.\n", omp_get_num_threads(), omp_get_thread_num());
    if (!pool->candidates[i]->frozen)
      step_route_assignment(pool->candidates[i], pool->scores[i], routing, use_dijkstra, early);
  }
}

struct cand_pool_t *
winnow_candidate_pool(struct cand_pool_t *pool,
		      int len, int topn, int topn_dup, char freeze, int iter) {
  struct cand_pool_t *ret;
  int             i,
                  j,
                  next,
                  from;

  assert(len >= topn * topn_dup);

  ret = init_candidate_pool(NULL, len);
  next = 0;
  for (i = 0; i < topn; i++) {
    from = get_next_low_score(pool);
    for (j = 0; j < topn_dup; j++) {
      ret->candidates[next] =
	  copy_route_assignment(pool->candidates[from]);
      ret->scores[next] = malloc(sizeof(struct score_t));
      assert(ret->scores[next]);
      *ret->scores[next] = *pool->scores[from];
      if (freeze && !j)
	ret->candidates[next]->frozen = 1;
      next++;
    }
  }
  assert(next <= len);

  for (; next < len; next++) {
    from = rand() % pool->len;
    ret->candidates[next] = copy_route_assignment(pool->candidates[from]);
    ret->scores[next] = malloc(sizeof(struct score_t));
    assert(ret->scores[next]);
    *ret->scores[next] = *pool->scores[from];
  }

  for (i=0; i<len; i++) {
    if (!ret->candidates[i]->frozen) {
      // Reseed by forcing at least a slight deviation from the initial value.
      // This avoids always executing with the same seed.
      //int tmp = rand_route(ret->candidates[i]);
      reseed_route(ret->candidates[i], iter*1000+i);
    }
  }

  ret->score_func = pool->score_func;
  memcpy(ret->params, pool->params, MAX_PARAM * sizeof(float));

  free_candidate_pool(pool);
  return ret;
}

void
free_candidate_pool(struct cand_pool_t *pool) {
  int             i;
  for (i = 0; i < pool->len; i++) {
    free_route_assignment(pool->candidates[i]);
    free(pool->scores[i]);
  }

  free(pool->candidates);
  free(pool->scores);
  free(pool);
}

void
set_score_func_by_name(struct cand_pool_t *cand, const char *name) {
  if (!strcmp(name, "linear"))
    cand->score_func = score_func_linear;
  else
    assert(0);
}

void
set_param(struct cand_pool_t *cand, int n, float param) {
  assert(n < MAX_PARAM);

  cand->params[n] = param;
}

void
init_place_all(struct cand_pool_t *pool) {
  int             i;

#pragma omp parallel for schedule(dynamic)
  for (i = 0; i < pool->len; i++) {
    //printf("Init-place with %d threads. This is thread %d.\n", omp_get_num_threads(), omp_get_thread_num());
    assign_drams(pool->candidates[i]);
    while(assign_node_spdram(pool->candidates[i]));

    if (i < pool->len/50) {
      while (assign_node_directed(pool->candidates[i], 1));
    } else {
      while (assign_node_random(pool->candidates[i]));
    }
    assign_all_routes_dijkstra(pool->candidates[i], routing_f);
  }
}

struct
score_slice_data {
  int ID;
  struct cand_pool_t *pool;
};

void*
score_slice(void *_dat) {
  int             i;
  struct score_slice_data *dat = _dat;

//#pragma omp parallel for schedule(dynamic) 
#pragma omp parallel for  schedule(dynamic)
  for (i = dat->ID; i < dat->pool->len; i += THREADS) {
    if (dat->pool->scores[i])
      free(dat->pool->scores[i]);
    dat->pool->scores[i] = score_route_assignment(dat->pool->candidates[i]);
    dat->pool->scores[i]->comp =
	dat->pool->score_func(dat->pool->scores[i], dat->pool->params);
    assert(dat->pool->scores[i]->comp >= 0);
    dat->pool->scores[i]->used = 0;
  }
  return NULL;
}

float
score_all(struct cand_pool_t *pool) {
  int             i;
  float           min;

#ifdef USE_PTHREADS
  pthread_t                  threads[THREADS];
  struct score_slice_data    args[THREADS];

  for (i=0; i<THREADS; i++) {
    args[i].pool = pool;
    args[i].ID = i;
    printf("Start scoring thread: %d\n", i);
    pthread_create(&threads[i], NULL, score_slice, (void*) &args[i]);
  }
  for (i=0; i<THREADS; i++) {
    pthread_join(threads[i], NULL);
  }
#else
  for (i = 0; i < pool->len; i++) {
    if (pool->scores[i])
      free(pool->scores[i]);
    pool->scores[i] = score_route_assignment(pool->candidates[i]);
    pool->scores[i]->comp =
	pool->score_func(pool->scores[i], pool->params);
    assert(pool->scores[i]->comp >= 0);
    pool->scores[i]->used = 0;
  }
#endif

  min = FLT_MAX;
  for (i = 0; i < pool->len; i++) {
    if (pool->scores[i]->comp < min)
      min = pool->scores[i]->comp;
  }
  return min;
}

void
summarize_scores(struct cand_pool_t *pool) {
  int             i;
  float           min,
                  max,
                  sum;
  float           cong,
                  hops,
                  ej_l,
                  inj_l,
                  tot_rt,
                  tot,
                  penalty_lim,
                  port_lim,
                  vcs;
  ej_l = inj_l = -1;
  cong = -1;
  hops = -1;
  tot = -1;
  tot_rt = -1;
  min = FLT_MAX;
  max = -FLT_MAX;
  sum = 0.0f;
  penalty_lim = 0.0f;
  vcs = 0.0f;

  //printf("\n");
  for (i = 0; i < pool->len; i++) {
    float           score = pool->scores[i]->comp;
    //printf("%8.2f", score);
    //if (i %10 == 0)
      //printf("\n");
    if (score < min) {
      min = score;
      cong = pool->scores[i]->worst_link_tokens;
      ej_l = pool->scores[i]->eject_port_lim;
      inj_l = pool->scores[i]->inject_port_lim;
      hops = pool->scores[i]->longest_route_hops;
      tot_rt = pool->scores[i]->total_route_hops;
      tot = pool->scores[i]->total_hops;
      penalty_lim = pool->scores[i]->link_penalty_lim;
      vcs = pool->scores[i]->vcs_needed;
    }
    if (score > max)
      max = score;
    sum += score;
  }
  //printf("\n");
  if (inj_l > ej_l)
    port_lim = inj_l;
  else
    port_lim = ej_l;

  printf("%6d scores: min %.2f \t%1.2f/%1.2f/%.0f/%1.2f/%.0f \tmax %.2f mean %.2f\n",
	 pool->len, min, cong/penalty_lim, port_lim/penalty_lim, hops, tot_rt/tot, vcs, max, sum / pool->len);
}
