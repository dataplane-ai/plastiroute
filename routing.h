#ifndef PLASTIROUTE_ROUTING_H
#define PLASTIROUTE_ROUTING_H

#include "types.h"

/* DETERMINISTIC ROUTING */

/* dimension-order routing (X first, then Y)*/
GList *route_dor_XY(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);

/* dimension-order routing (Y first, then X)*/
GList *route_dor_YX(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);

/* dimension-order routing */
GList *route_dor(
	struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);

/* Balanced DOR for k-ary n-cubes
 * brief: BDOR is a deterministic routing algorithm
 *        that tries to balance load for concentrated traffic patterns.
 *        BDOR accomplishes this by doing DOR_XY or DOR_YX based on 
 *        source and destination information. Therefore, BDOR is deterministic.
 * @note: normally, BDOR requires two VC classes (one for DOR_XY, one for DOR_YX)
 *        to be deadlock-free.
 */
GList *route_bdor(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);

/* ADAPTIVE ROUTING */

/* Adaptive routing with local channel info.
 * @brief: this routing algorithm chooses the next hop
 *         based on local channel information.
 *         The algorithm is minimal routing because it chooses hops only in the 
 *         quadrant defined by source and destination nodes.
 */
GList *route_min_local(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);

/* directed minimal valient routing
 * This is similar to route_valient with the difference that
 * the best minimal intermediate node is used.
 */
GList *route_min_directed_valient(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);


/* directed valient routing
 * This is similar to route_valient with the difference that
 * the best intermediate node is used.
 */
GList *route_directed_valient(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
);

/* OBLIVIOUS ROUTING */

/* Valient routing 
 * intermediate node can be any node
 */
GList *route_valient(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
); 

/* Minimal valient routing 
 * intermediate node is chosen within the quadrant defined
 * by source and destination nodes
 */
GList *route_min_valient(
    struct link_t* links,
    struct mesh_loc_t from,
    struct mesh_loc_t to,
    int num_cols,
    int num_rows,
    struct route_t *route
); 


#endif /* PLASTIROUTE_ROUTING_H */

