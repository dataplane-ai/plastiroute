#ifndef DOT_H
#define DOT_H

#include "gene.h"
#include "types.h"

void
dot_print_assigned_routes(const char *file, struct route_assignment_t *route);

void
place_print_assigned_nodes(const char *file, struct route_assignment_t *route);

void
write_summary_line(const char *file, const char *key, int iter,
    struct cand_pool_t *cand, int endp_vcs, int color_vcs, int usec);

void
write_tungsten_file(const char *file, struct route_assignment_t *route, const char *tungsten_prefix);

int
dot_lin_row_col(int row, int col, int nrow, int ncol);

#endif
